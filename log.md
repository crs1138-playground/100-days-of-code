# 100 Days Of Code - Log

## Day 39: August 20, 2019

**Today's progress:** Drupal 8 Site Building

**Thoughts:** Today I finished the course. The next step is to download Acquia's
study plan for the Theming certification and start on that.

## Day 38: August 19, 2019

**Today's progress:** Drupal 8 Site Building

## Day 37: August 18, 2019

**Today's progress:** Drupal 8 Site Building

**Thoughts:** I've covered quite a big chunk of the D8 Site Building course.
Again, there wasn't anything radicaly new, but it's a good revision exercise
before I move onto the exam's curriculum.

### Notes

#### Fielded Content Structures
1) Content Entities
2) Configuration Entities

**Content Entities**
* custom fields
* stored in db tables
* mostly created on front-end
* examples: nodes, custom blocks, users, taxonomy terms, menu links, feeds

**Configuration Entities**
* can deploy to different environments
* stored in the config system
* mostrly created in admin
* examples: content types, custom block types, user roles, views, taxonomy
  vocabularies, menus, image styles


## Day 36: August 12, 2019

**Today's progress:** Drupal 8 Site Building

## Day 35: August 11, 2019

**Today's progress:** Drupal 8 Site Building

## Day 34: August 10, 2019

**Today's progress:** Drupal 8 Beginner course; Drupal 8 Site Building

**Thoughts:** I've whizzed through the rest of Acquia's Drupal 8 Beginner 
course. Nothing realy new, but I still have the sense of accomplishment even if 
I was just refreshing what I've learnt over the last two years.

## Day 33: August 9, 2019

**Today's progress:** Drupal 8 Beginner course

**Thoughts:** I decided to regress even further and actualy go through Acquia's
Drupal 8 Beginner course. I skimmed through the first 15 lessons within an hour.

### Notes

#### Views
1. Display
2. Format
3. Fields
4. Filter
5. Sort

## Day 32: August 8, 2019

**Today's progress:** Drupal 8 Beginner course

**Thoughts:** I decided to regress even further and actualy go through Acquia's
Drupal 8 Beginner course. I skimmed through the first 15 lessons within an hour.

### Notes

* `node` = every Drupal content item
* `content type` = In Drupal, each item of content is a called a node and each 
  node belongs to a single content type, which defines various default settings 
  for nodes of that type, such as whether the node is published automatically 
  and whether comments are permitted.
  - also controls how content is added to the site
* `view mode` aka `layout` = types of displays shared across content entities
* `view display` = the way CT fields are displayed, these are defined in `view
  mode`
* `views` = Selects, orders, and presents your content or any part/combination 
  of it based on the criteria you define.

## Day 31: August 7, 2019

**Today's progress:** Drupal 8 Theming course – Module 4 Creating a sub-theme

**Thoughts:** Turns out that this course was a bit of a recap and the bits and 
bobs I really need to learn for the certification exams were not covered here.
It did give me some very useful pieces of information in terms of, _how to go
about making a new (sub-)theme_. I feel, I really should do the **Drupal 8 Site
Building Course** in order to grasp some basic concept, that I've learnt on the
fly over the last two years. Fundamental things, like what exactly is a node, 
etc. :)

### Notes

#### Drupal 8 CSS Best Practices
* SMACSS categorization
  * Base - HTML element
  * Layout - page layout styling
  * Component - styling for re-usable UI components
  * State - styling for client-side changes (ie. mouseovers)
  * Theme - look and feel of components

Drupal agregates CSS files, so multiples won't hinder speed on load =>
Minimum files:
* `base.css`
* `layout.css`
* `components.css` 
  - for more complex theme, break down components into 
   component families, where you have a file for each component
  - state rules should be bundled with the component they affect. This includes
    media queries.
  - theme rules should also be bundled with component unless the theme is a base
    or a starter, then it's recommended to give them their own files


## Day 30: August 6, 2019

**Today's progress:** Drupal 8 Theming course – Module 4 Creating a sub-theme

**Thoughts:** I've spent an hour of googling around and reading through various
forums. Finally, I found the solution. Turns out the root of the problem is 
Acquia's default settings for the site. It makes me wonder, how a company that
charges substantial money for issuing Drupal certificates can launch a tutorial
that has such serious flaws and yet they expect to be taken seriously.

## Day 29: August 5, 2019

**Today's progress:** Drupal 8 Theming course – Module 4 Creating a sub-theme

**Thoughts:** My frustration with Drupal is that even when I follow a course 
step-by-step, I end up getting different results. This time the images of the 
Services CT are not showing after adding new content. They just end up in a 
different folder than Drupal is looking for them. What do I do now?! I wrote to 
Acquia, hoping they might get back to my comment on YT. But it's about 50/50 
whether they even acknowledge my comment.

## Day 28: August 4, 2019

**Today's progress:** Drupal 8 Theming course – Module 3 Sub-theming

### Notes

#### Inherited in Sub-theme
* `.theme`
* `.breakpoints.yml`
* Templates
* `.libraries.yml`
* Images - inheritted as long as `.libraries.yml` is present in base theme
* CSS - inheritted as long as `.libraries.yml` is present in base theme
* Javascript - inheritted as long as `.libraries.yml` is present in base theme
* Screenshot

#### Not inherited
* `.info.yml` - every project needs its own
* `logo.svg`
* `favicon.ico`
* Regions ???
* Color Module - color module related configuration and assets are not inherited
  by a subtheme.

## Day 27: August 3, 2019

**Today's progress:** Drupal 8 Theming course – Module 2

**Thoughts:** This is still a recap of what I've been using for the last 2 years 
working for Netuxo. However, I'm going through the tutorial making sure that 
none of the potentially crucial information escapes me. The pitfalls of being a 
self-taught developer.

### Notes

#### Drupal Template Hierarchy
* Templates render from the most specific to least specific
  ```
  field.html.twig >
    node.html.twig >
      region.html.twig >
        page.html.twig >
          html.html.twig
  ```
* `html.html.twig` - declares the head and the body, HTML and Twig syntax and
  document type
* `page.html.twig` - implement regions and their wrappers
  ```twig
  {% if page.sidebar_first %}
    <div id="sidebar-first" class="column sidebar">
      <aside class="section" role="complementary">
        {{ page.sidebar_first }}
      </aside>
    </div>
  {% endif %}
  ```
* `region.html.twig` - renders the region themselves
* `block.html.twig` - used to mark up blocks within regions
* `node.html.twig` - builds the node area
* `field.html.twig` - markup for fields nested within nodes
* It's discouraged to create markup in module PHP files, use Twig instead.
* [Twig markup for modules](https://www.drupal.org/node/2640110)

#### Template Overrides

1. Find the template you want to override
2. Make a copy of it in your theme's `/templates/` folder.
3. [Twig Template naming conventions](https://drupal.org/node/2354645)

#### Twig Debugging
* Turn on debugging in `/sites/default/services.yml` by `debug: true`.
* Twig suggestions are ordered from the most specific at the top to the least 
  specific at the bottom.
* Debug comments also show what Twig file is currently being used to render.
* Further info: [Debugging compiled Twig templates](https://drupal.org/node/1903374)
* Useful debugging syntax:
  ```twig
  {{ dump() }}
  {{ dump(var) }}
  {{ dumb(base_path) }}
  ```
  [Dump syntax in twig](https://twig.sensiolabs.org/doc/functions/dump.html)
  
#### Devel project

* [Devel](https://www.drupal.org/project/devel)
* Allows generating content (user, nodes, taxonomies, …)
* Provides _Kint_ - more readable way of displaying variables in use in Twig 
  templates.

#### Adding regions

1. Three files you'll edit to create a region
  * declare regions in `mytheme.info.yml`
  * render region in `page.html.twig`
  * style region with CSS
2. Clear your cache after adding regions!
3. Check _Structure > Block layout_

**Useful link:** [Drupal Theming Guide](https://drupal.org/theme-guide/8)

## Day 26: August 2, 2019

**Today's progress:**  Drupal 8 Theming course – Module 2

**Thoughts:** I'm feeling patient today and I'm really trying hard to put the 
time in and read the basic docs of Drupal. I'm starting to realise the 
complexity of Drupal and the need to read a lot of documentation, which is not
always very well written. The patience is the key.

**Link to the course:** [Drupal 8 Layout and Theming, Lesson 21: Anatomy of a Theme](https://youtu.be/w51qZKT4Gf0)

### Notes:

#### Drupal Coding Standards
* use and indent of 2 spaces, with no tabs
* lines should have no trailing spaces
* files should be formatted with Unix line endings ("\n")
* don't use windows line endings ("\r\n")
* lines should not be longer than 80 characters (generally)
* [Drupal Coding Standards](http://drupal.org/coding-standards)

#### Twig
* use as space after an opening delimiter and before a closing delimiter
  ```twig
  {% if page.header %}
    {{ page.header }}
  {% endif %}
  ```
* put one space after the use of `:` or `,` in arrays or hashes
* do not put a space between open and closing parentheses in expressions
  ```twig
  {{ 1 + (2 * 3) }}
  ```
* do not put a space between string delimiters
  ```twig
  {{ 'hey' }}
  {{ "hello" }}
  ```
#### Yaml
* the _file.yml_ files do not accept tab characters and will throw error if they
  are present
* indents should be two spaces long, per indentation
* when encountering error, check **Reports > Recent log messages** at Drupal 
  admin

#### Benefits of Coding Standards
* Cleaner and easier to read code, matching the style of the rest of the 
  codebase
* The coding style decisions are made for you
  * [Twig](https://www.drupal.org/node/1823416)
  * [PHP](https://www.drupal.org/coding-standards)
  * [CSS](https://www.drupal.org/node/1886770)
  * [API & Commneting](https://www.drupal.org/node/1354)

## Day 25: August 1, 2019

**Today's progress:** Drupal 8 Theming course – Module 1

**Thoughts:** Today it was a bit of an emotional rollercoaster. At first, I 
found that part of Acquia's course video is missing, then they were asking me to 
use files that were supposed to come with the course, but there was no mention 
of any download anywhere to be found. I had a quick look at the comments on 
their YT channel just to find out, that I'm not the only one. Other people were 
experiencing the same frustration as I did. Yet their calls were unanswered.

I made the effort and wrote another comment, being sceptical, that I'd ever 
heard back from Acquia. But they did. They posted the link to the course files 
on the first video of the course. That felt very positive like I've just done 
something other people and I can benefit from.

**Link to the course:** [Acquia Drupal 8 Layout & Theming Course](https://www.youtube.com/playlist?list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng)

### Notes:

#### Regions

If you define _ONE custom region_, you need to declare all.
* Declare in `theme.info.yml`, 
* Render `page.html.twig`,
* Style with CSS
* Two hidden regions: `page_top` and `page_bottom`, special case for use by 
  Drupal. If we declare a region we MUST declare these two.
  > Make sure you keep the page_top and page_bottom regions.  These are 'hidden' 
  regions, used for markups at the very top and bottom of the page, such as 
  analytics or the admin toolbar.  You don't need to list them in your 
  _THEMENAME.info.yml_ file, just don't remove them from the _html.html.twig_ 
  template. Modules may rely on them being present. 
  -> https://www.drupal.org/docs/8/theming/adding-regions-to-a-theme
 
* The order we declare regions is important.

#### Libraries

* Create `theme.libraries.yml` and reference it in `theme.info.yml`.
  ```yml
  libraries:
    - mytheme/global-css
    - mytheme/pacifico
  ```
* Define containers `global-css` and `pacifico` with further details in 'theme.libraries.yml'.
  ```yml
  global-css:
    version: VERSION
      css:
    theme:
      css/elements.css: {}
      css/layout.css: {}
      css/print.css: { media: print }
  pacifico:
    license:
      name: SIL Open Font License 1.1
      url: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi$id=OFL
    css:
      theme:
        //fonts.googleapis.com/css?family=Pacifico&display=swap: { type: external }
        css/styles.css: {}
  ```
* Advanced `theme.info.yml` options
  * **libraries extend**
    ```yml
    libraries-extend:
      core/drupal.user:
        - mytheme/user1
        - mytheme/user2
    ```
  * **libraries override**
    ```yml
    libraries-override:
      # Replace entire library.
      core/drupal.collapse: mytheme/collapse
    ```
  * **stylesheet-remove**
    ```yml
    stylesheet-remove:
      - core/assets/vendor/normalize-css/normalize.css
    ```
#### Breakpoints

Define breakpoints for media queries to use in responsive design. Breakpoints 
are defined in `.breakpoint.yml` file and have syntax of 
`projectname.descriptor` for example `bartik.mobile` or `bartik.narrow`:
```yml
mytheme.mobile:
  label: mobile  # human friendly description
  mediaQuery: '' # specify the actual media query
  weight: 0      # order of the breakpoints
  multipliers:   # pixel resolution multipliers
    - 1x
mytheme.narrow:
  label: narrow
  mediaQuery: 'all and (min-width: 560px) and (max-width: 860px)'
  weight: 1
  multipliers:
    - 1x
mytheme.wide
  label: wide
  mediaQuery: 'all and (min-width: 861px)'
  weight: 2
  multipliers:
    - 1x
```


## Day 24: July 31, 2019

**Today's progress:** Drupal 8 Theming course - Module 1

**Thoughts:** I've made a start on Acquia's Drupal 8 course for front-end 
developers. I went quickly through the first 9 lessons as it's nothing new.

This is a new experience for me. The cooperative, I work with, is paying me to 
take an official paid certification exam. I feel obliged to do well in this, so 
I've started a course designed by Acquia, the company that does the examination. 
This should keep me busy for at least a couple of weeks.

**Link to the course:** [Acquia Drupal 8 Layout & Theming Course](https://www.youtube.com/playlist?list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng)

### Notes:

* Drupal theming: _everything is an override of what Drupal gives you.
* Chain of command: `PHP > HTML > CSS + JS`\
* It is not possible to use PHP straight in the template files
* Theme folder: `/themes/mytheme/`
* Types of themes:
  * _core themes_: Drupal core themes
  * _contributed themes_: these have been contributed back to the Drupal Community - https://drupal.org/project/themes
  * _contributed starter/base themes_: starting points for a custom theme
  * _custom themes_
  * _contributed admin themes_: displayed only in the administration of a site
  * _premium themes_: premade themes available for purchase
* Keep separate contributed themes and custom themes separated
* Core themes: `/core/themes/bartik/` - never change core files
* Static vs Configurable Themes
  * _static_ - default settings only
  * _configurable_ - theme images, layout, media queries, external font libs, …

**Custom theme naming convention:**
* Name must be unique (if it is to be contributed to Drupal Community)
* Conventions:
  * all lower case beginning with a letter
  * no spaces, dashes, punctuation
  * can contain underscores and numbers if not leading with one
  * name of the project needs to match its folder name
  * **.info.yml** - the only required file of a theme. It declares the theme label, type of project, core compatibility, regions and feature overrides, location of resources (CSS, JS)

## Day 21-23: July 29, 2019

**Today's progress:** FCC - Javascript Algorithms And Data Structures Certification

**Thoughts:** I quit on daily logging. I was trying to juggle family, work, 
coding challenge and fitness challenge. This was causing me too much stress and 
I decided to carry on with the actual coding, but write only when I feel a 
strong impulse to do so.

Today, I feel great satisfaction as I finally managed to finish the 
@freeCodeCamp's "Javascript Algorithms And Data Structures Certification". Yay!

**Link to the certificate:** [Javascript Algorithms And Data Structures Certificate](https://www.freecodecamp.org/certification/crs1138/javascript-algorithms-and-data-structures)

## Days 18–20: July 21–23, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I've been continuing in the FCC JS challenges, not much to report
apart of some winging about being a poor mathematician.

## Day 17: July 20, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I had a good day today. I managed to solve two challenges. That 
boosted my self-confidence. I was focusing on using ES6.

**Link to work:** [Sorted Union](https://codepen.io/crs1138/pen/zgGwoJ)

## Day 16: July 19, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I confess, today I cheated.  I copied the code from the provided 
solution. I can write code. However, I'm not much of a maths boffin, thus these 
recent challenges on the @FreeCodeCamp – JS/Intermediate Algorithm Scripting are 
getting me down. I find them distant from reality. Perhaps I'm lucky and when I 
need math I can call for help from people that enjoy this. The problem gets 
solved.

In my defence, I did debug the code line by line until I understood, what it's 
doing and why. It made me feel inferior.

**Link to work:** [Smallest Common Multiple](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/smallest-common-multiple)

## Day 15: July 18, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I always check the 'Get a hint' after I solve the challenge. I 
like the elegance of the Advanced Solutions presented there. My brain just does 
not think that way. I wonder if there is a way to train and practice that kind 
of thinking.

**Link to work:** [The comparison of my code vs the Advanced Solution](https://codepen.io/crs1138/pen/dxyOrW)

## Day 14: July 17, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I got it done, but it was a struggle. I need to get back to the 
roots. In the old days, I was using flowcharts to get the logic right. Does 
anybody know a free online app for creating flowcharts? I've tried a few, but 
none of them is very fluid when it comes to making flowcharts.

**Link to work:** [FCC - Intermediate Algorithm Scripting: Wherefore art thou](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/wherefore-art-thou/)

## Day 13: July 16, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** I'm getting behind. When I don't get to do my challenge as the 
first thing in the morning, then I end up doing as the last thing. Not only, my
brain is getting slow by then, but I am also creating a vicious circle. I work
on my challenge late at night, thus I oversleep in the morning, which means I 
end up doing it at night. And so on…

**Link to work:** [FCC - Intermediate Algorithm Scripting: Seek and Destroy](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/seek-and-destroy)

## Day 12: July 15, 2019

**Today's progress:** FCC Intermediate Algorithm Scripting

**Thoughts:** Late start today meant I didn't get around to code until after 
work. I was already tired by then as my work is coding anyway, so I didn't get 
much done. Never mind, tomorrow's another day. It is not a race. Learning is 
what counts!

## Day 11: July 14, 2019

**Today's progress:** FCC Functional Programming

**Thoughts:** I've finished the Functional Programming, I was expecting it to be
more difficult, more about closures, etc. So in the end this was actually pretty
fast.

**Link to work:** [Functional Programming](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/functional-programming)

### Notes

* In _functional programming_, changing or altering things is called `mutation`, 
and the outcome is called a `side effect`. A function, ideally, should be a 
`pure function`, meaning that it does not cause any side effects.
* The `arity` of a function is the number of arguments it requires. `Currying` a 
function means to _convert a function of `N arity` into `N functions of arity 1`._
* `Partial application` can be described as applying a few arguments to a 
function at a time and returning another function that is applied to more 
arguments.

## Day 10: July 13, 2019

**Today's progress:** FCC Functional Programming

**Thoughts:** Today was hard. I've changed my routine and did my 
\#100DaysOfFitness as the first thing in the morning, thinking I'd do the coding 
challenge for the day later on. I did not account for the heat of the summer in 
Spain. I know, silly me, I have lived here for only about 14 summers and still 
got caught unprepared. I am lacking confidence when it comes to functional 
programming as it can get quite complex and with the heat around 17.00, it was 
just not getting in. I had a break and went to do other stuff until it cooled 
down a bit and finished off my hour of code for today at 23.30.

**Link to work:** [Functional Programming](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/functional-programming)

**Tweet:** Today was hard. The heat of the summer got to me. I changed my 
routine and tried to focus on coding in the PM. I ended up waiting until the 
temperature dropped in the evening. I spent that time with my wife instead, 
chatting, doing some little jobs around the house.

## Day 9: July 12, 2019

**Today's progress:** FCC Object Oriented Programming

**Thoughts:** I might be having that kind of _aha_ moment. I've gone through all
the exercises in the OOP chapter and right at the end is explained, how modules 
are made using the Immediately Invoked Function Expression (IIFE). Now, that is 
a pattern, I've been using for a while without actually understanding what is 
going on there and why. I have always struggled to get the syntax right. This 
last challenge in the chapter deffo helped to clear that out. Now, I'm just not 
sure if this pattern is still necessary now in the age of ES6 and different JS 
compilers such as Babel. But it is progress nevertheless.

There are basic questions that remain unanswered:
* Why would I use that instead of defining an object with methods?
* Would I use this pattern inside of ES6 modules with the default export?
* Is it not just a legacy pattern?


**Link to work:** [Object Oriented Programming](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/object-oriented-programming)

### Notes:

The way to construct a module is to:
* declare its name
* assign it the **IIFE** that **returns an object**
* define the object properties and methods
* call the module using the object dot notation
```
const motionModule = (function(){
    return {
        fly: function() { … },
        drive: function() { … },
        walk: function() { … }
    };
})();

motionModule.drive();
```

## Day 8: July 11, 2019

**Today's Progress:** FCC Basic Algorithm Scripting – done&dusted

**Thoughts:** I'm happy, I managed to finish all the challenges of the Basic 
Algorithm Scripting chapter. I'm also puzzled with the last one of them. I got 
there in the end, that's ok. But when I looked up other solutions, I found a 
more elegant solution using a recursive function call. My brain just doesn't 
work that way. It makes me feel inferior coder. Just a wee bit.

**Link to work:** [Basic Algorithm Scripting](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-algorithm-scripting/)

## Day 7: July 10, 2019

**Today's Progress:** FCC Basic Algorithm Scripting

**Thoughts:** Today's start was a bit hazy as my brain doesn't kick in straight 
away after waking up. But I got there in the end. I feel like I should be faster
at solving basic algorithms, but then there's no time limit, so I'm happy to let
it flow as it comes.

**Link to work:** [Basic Algorithm Scripting](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-algorithm-scripting/)

## Day 6: July 9, 2019

**Today's Progress:** FCC Basic Algorithm Scripting

**Thoughts:** The game of the day - simple algoritm. It showed me, that not all
of them are that simple. I had to look up a flowchart for factorial as I was
overcomplicating it by trying to solve it using reciprocal function call.

**Link to work:** [Basic Algorithm Scripting](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-algorithm-scripting/)

## Day 5: July 8, 2019

**Today's Progress:** I finished the the FCC Basic Data Structures

**Thoughts:** Today, I revised the JS basic data structures - arrays and
objects. I'm starting to think, that perhaps I should be doing some more
challenging tasks. Then again, part of the reason, I started with the FCC 
curriculum is to get their certifications. So, I'll stick to this until it gets
more tricky.

**Link to work:** [Basic Data Structures](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-data-structures)

## Day 4: July 7, 2019

**Today's Progress:** JS Debugging, Basic Data Structures

**Toughts:** I've started off with some basic JS debugging. It was easy as I use 
this practically on a daily basis at work. Thus I finished this quickly.

As the debugging was really an easy recap, I took on the next set of challenges
related to the **Basic Data Structures**. Again, just a recap, but I need to do
this in get my JS certification. After all _repetitio est mater studiorum…_

**Link to work:** [Debugging](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/debugging)\
**Link to work:** [Basic Data Structures](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/basic-data-structures)

### Notes:

* There are three basic types of errors:
  1) **syntax error** - spelling, missing closing brackets, quotation marks 
     mismatch, etc.
  2) **runtime error** - often bad logic leading to problems like infinite loops
  3) **semantic error** - mistakes in the actual logical structure of the code, 
     ie. multiplying when we want to add up, etc.


## Day 3: July 6, 2019

**Today's Progress:** I finished the rest of the ES6 and spent the remaining 
time with regex. I managed to get through the whole chapter, mainly because I 
didn't want it to spill into another day. It's done and dusted.

**Toughts:** I find regex a real antonym to intuitive. The syntax is complicated
and sometimes it is quite difficult to think of all the possible implications. 
I had to look up a couple of solutions to the FCC Regex challenges. This wasn't 
my first insight into regex, but I did. Phew… 

For me, regex is like a tax return, when I need to do it, I'll struggle through, 
but there is no joy even when I eventually get the results. 

I like to use [Regular Expressions 101](https://regex101.com/) to help me with 
regex-related tasks.

I enjoy the #100DaysOfCode challenge on working days, but it's hard to do it 
over a weekend.

**Link to work:** [Introduction to the Regular Expression Challenges](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/regular-expressions)

### Notes:

* Today I finished the ES6 section of the FCC JS curriculum, revising all the 
different ways how to pass code between JS modules.
* **Greedy expression** - matches the **longest possible** part of a string.
* **Lazy expression** - matches the **shortest possible** part of a string.
* Regexp by default are greedy.
* Capture groups can be repeated in the same expresion using `\<int>` ie. `\1` , 
but it they will then test positive only what they found the first time. 
For example:
```
let str1 = '42 42 42';
let str2 = '10 12 13';
const matchReg = /(\d+)\s\1\s\1/;
let result = matchReg.test(str1); // true
result = matchReg.test(str2);     // false
```

## Day 2: July 5, 2019

**Today's Progress:** FCC curriculum - ES6, I started off by learning 
destructuring in more detail [Use Destructuring Assignment to Assign Variables from Nested Objects](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/use-destructuring-assignment-to-assign-variables-from-nested-objects). 
I finished with [Understand the Differences Between import and require](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/understand-the-differences-between-import-and-require).

**Toughts:** Revising is like watching a film yet another time. One tends
to read fast, missing details. It is the details that make a difference when
trying to solve the real-world coding problems. It takes discipline to read a 
new text about a topic, that I've learnt before, with a meticulous focus.

**Link to work:** [Use Destructuring Assignment to Assign Variables from Nested Objects](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/use-destructuring-assignment-to-assign-variables-from-nested-objects)

### Notes:

* Swapping values of variables `a => b` and `b => a`, I can use array 
destructuring `[b, a] = [a, b];`. It's important to do so
without the `const` keyword. The following statement will NOT work 
`const [b, a] = [a, b];`.
* I shall remember that **template literals** `` `Max: ${max}` ``can span across 
multiple lines and the line breaks will stay in the output.
* The difference between **require()** and **import** is that `import` lets us 
to import only selected functions.

## Day 1: July 4, 2019

**Today's Progress:** Continue the FreeCodeCamp curriculum - ES6, I've started
off with the [PassedDeclare a Read-Only Variable with the const Keyword](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/declare-a-read-only-variable-with-the-const-keyword)
and work my way through all the way to [Use Destructuring Assignment to Assign Variables from Objects](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/use-destructuring-assignment-to-assign-variables-from-objects)

**Thoughts:** I am starting of with easy challenges where I should really know 
the answers, I've decided to experiment a bit more than usual. The code I wrote
might not be the most straight forward answers to the FCC challenges, but it
works and it helps me to extend my understanding.

**Link to work:** [FreeCodeCamp.Javascript.ES6.const](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/es6/declare-a-read-only-variable-with-the-const-keyword)

### Notes:

* When declaring variables in ES6 using const you can change the content of
arrays and properties of objects. But you can also mutate them using standard
functions or methods ie `myArray.push()` or `myArray.shift()`
* I've noticed that many challenges on FCC are using closure statements for 
declaring the functions inside of a SIAF. I'm not quite sure, why that is.

